DS BDA Lab2: Spark
=====
## Task

### Business logic

Program which aggregate raw metrics into selected scale
Data input format: metricId, timestamp, value
Data output format: metricId, timestamp, scale, value
For example:
1, 1510670916247, 10		1, 1510670880000, 1m, 30
1, 1510670916249, 50		2, 1510670880000, 1m, 20
1, 1510670916251, 50	🡪
2, 1510670916253, 20
1, 1510670916255, 10


### Ingest technology

sqoop importer

### Storage technology

cassandra

### Computation technology

Spark RDD

### Report includes

1. ZIP-ed src folder with your implementation
2. Screenshot of successfully executed tests
3. Screenshots of successfully executed job and result (logs)
4. Quick build and deploy manual (commands, OS requirements etc)
5. System components communication diagram (UML or COMET)

## System components communication diagram

![System components communication diagram](./SystemComponentsCommunicationDiagram.png)

## Links

[Build and deploy manual](./BuildAndDeploy.md)

[Screenshots](./screenshots/README.md)
