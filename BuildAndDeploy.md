Build and deploy manual
=======================

## Required tools

- jdk (version 7 or later)
- maven
- docker
- docker-compose
- git

## Preparation

Download spark binaries (required only to submit job)

```bash
wget https://archive.apache.org/dist/spark/spark-2.2.0/spark-2.2.0-bin-hadoop2.7.tgz
tar -xvf spark-2.2.0-bin-hadoop2.7.tgz
```

Download project sources

```bash
git clone git@bitbucket.org:dvalter/mephi-ds-bda-hw2.git
cd mephi-ds-bda-hw2
```

## Build

Build application with maven

```bash
mvn package
```

Build docker images

```bash
cd docker
docker-compose build
```

## Start system components

```bash
cd docker
docker-compose up -d
```

## Deploy

### Prepare Cassandra and Clickhouse

Create tables

```bash
./scripts/boostrap_db.sh

```

Generate test data

```bash
mkdir test_data
scripts/generate_data.sh 4000 > test_data/data
```

Load data to Clickhouse

```bash
./scripts/load_data.sh test_data/data
```


## Transfer data to Cassandra via Sqoop

```bash
docker exec -it docker_sqoop_1 sqoop-transfer logs timeseries timeseries raw_metrics
```

### Submit spark job

Set cassndra seed node and spark master ip

```bash
export SPARK_MASTER_URL=spark://$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' docker_spark-master_1):7077
export CASSANDRA_URL=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' docker_cassandra_1)
```

Submit job to spark cluster (set SPARK_INSTALL_DIR to spark directory)

```bash
SPARK_DIR="path to your spark directory"
"$SPARK_DIR"/bin/spark-submit --class org.bitbucket.dvalter.dsbda.hw2.SparkApplication --master "$SPARK_MASTER_URL" ./target/hw2-1.0-SNAPSHOT-jar-with-dependencies.jar  30d
```

## Check results

```bash
docker exec -it docker_cassandra_1 cqlsh localhost -e 'SELECT * from timeseries.aggregated_metrics'
```
