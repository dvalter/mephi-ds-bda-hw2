package org.bitbucket.dvalter.dsbda.hw2;

import org.bitbucket.dvalter.dsbda.hw2.util.TimeUtil;
import org.junit.Test;

import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class TimeUtilTest {
    @Test
    public void validTimeRoundingTest() {
        TimeUtil util = new TimeUtil("1ms");
        for (int i = 0; i < 100; i++) {
            long t = ThreadLocalRandom.current().nextLong(10000000);
            assertEquals(t, util.roundTime(t));
        }

        util = new TimeUtil("2ms");
        for (int i = 0; i < 100; i++) {
            long t = ThreadLocalRandom.current().nextLong(10000000);
            assertEquals((t / 2) * 2, util.roundTime(t));
        }

        util = new TimeUtil("1s");
        for (int i = 0; i < 100; i++) {
            long t = ThreadLocalRandom.current().nextLong(10000000);
            assertEquals((t / 1000) * 1000, util.roundTime(t));
        }

        util = new TimeUtil("13s");
        for (int i = 0; i < 100; i++) {
            long t = ThreadLocalRandom.current().nextLong(10000000);
            assertEquals((t / 13000) * 13000, util.roundTime(t));
        }

        util = new TimeUtil("2m");
        for (int i = 0; i < 100; i++) {
            long t = ThreadLocalRandom.current().nextLong(10000000);
            assertEquals((t / (2 * 60 * 1000)) * (2 * 60 * 1000), util.roundTime(t));
        }

        util = new TimeUtil("4h");
        for (int i = 0; i < 100; i++) {
            long t = ThreadLocalRandom.current().nextLong(10000000000L);
            assertEquals((t / (4 * 60 * 60 * 1000)) * (4 * 60 * 60 * 1000), util.roundTime(t));
        }

        util = new TimeUtil("1d");
        for (int i = 0; i < 100; i++) {
            long t = ThreadLocalRandom.current().nextLong(10000000000L);
            assertEquals((t / (24 * 60 * 60 * 1000)) * (24 * 60 * 60 * 1000), util.roundTime(t));
        }
    }

    @Test
    public void invalidScaleTest() {
        assertThrows(IllegalArgumentException.class, () -> new TimeUtil("1dd"));
        assertThrows(IllegalArgumentException.class, () -> new TimeUtil("ad"));
        assertThrows(IllegalArgumentException.class, () -> new TimeUtil("ms"));
        assertThrows(IllegalArgumentException.class, () -> new TimeUtil("h"));
        assertThrows(IllegalArgumentException.class, () -> new TimeUtil("1mh"));
        assertThrows(IllegalArgumentException.class, () -> new TimeUtil("1hd"));
    }
}
