package org.bitbucket.dvalter.dsbda.hw2;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.bitbucket.dvalter.dsbda.hw2.dto.AggregatedMetric;
import org.bitbucket.dvalter.dsbda.hw2.dto.InputMetric;
import org.bitbucket.dvalter.dsbda.hw2.timeseries.MetricAggregator;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class TimeSeriesTest {
    private JavaSparkContext sparkCtx;
    private List<InputMetric> input;
    private List<AggregatedMetric> expectedOutput;
    private final long ONE_HOUR = TimeUnit.HOURS.toMillis(1);
    private final String SCALE = "1h";

    @Before
    public void init() throws IllegalArgumentException {


        SparkConf conf = new SparkConf();
        conf.setMaster("local[2]");
        conf.setAppName("junit");
        sparkCtx = new JavaSparkContext(conf);

        input = new ArrayList<>();
        expectedOutput = new ArrayList<>();
        int size = ThreadLocalRandom.current().nextInt(128) + 128;
        int baseId = 0;
        for (int i = 0; i < size; ++i) {

            int id = baseId + ThreadLocalRandom.current().nextInt(10) + 1;
            baseId = id;
            long baseHour = 1000;

            int sizePerId = ThreadLocalRandom.current().nextInt(16) + 16;
            for (int j = 0; j < sizePerId; j++) {
                long hour = baseHour + ThreadLocalRandom.current().nextInt(32) + 1;
                baseHour = hour;
                int sizePerHour = ThreadLocalRandom.current().nextInt(15) + 1;
                int total = 0;

                for (int k = 0; k < sizePerHour; k++) {
                    long timestamp = ThreadLocalRandom.current().nextLong(ONE_HOUR) + ONE_HOUR * hour;
                    int value = ThreadLocalRandom.current().nextInt(100);
                    input.add(new InputMetric(id, new Date(timestamp), value));
                    total += value;
                }
                expectedOutput.add(new AggregatedMetric(id, new Date(hour * ONE_HOUR), SCALE, total / sizePerHour));
            }
        }
    }

    @Test
    public void test() {
        JavaRDD<InputMetric> metricsRDD = sparkCtx.parallelize(input, 1);
        JavaRDD<AggregatedMetric> res = MetricAggregator.aggregateMetrics(metricsRDD, SCALE);
        List<AggregatedMetric> reslist = res.collect();
        assertEquals(expectedOutput.size(), reslist.size());
        assertTrue(reslist.containsAll(expectedOutput));
    }
}