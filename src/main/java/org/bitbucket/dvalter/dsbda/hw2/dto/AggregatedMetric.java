package org.bitbucket.dvalter.dsbda.hw2.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Aggregated value
 * @author Dmitry Valter
 * @since 7/11/2020.
 */
@Data
@AllArgsConstructor
public class AggregatedMetric implements Serializable {

    // ID
    private Integer id;

    // timestamp
    private Date time;

    // scale
    private String scale;

    // value
    private Integer value;
}