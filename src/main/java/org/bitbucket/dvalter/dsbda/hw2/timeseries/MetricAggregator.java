package org.bitbucket.dvalter.dsbda.hw2.timeseries;

import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import com.datastax.spark.connector.japi.CassandraJavaUtil;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.bitbucket.dvalter.dsbda.hw2.dto.AggregatedMetric;
import org.bitbucket.dvalter.dsbda.hw2.dto.InputMetric;
import org.bitbucket.dvalter.dsbda.hw2.util.TimeUtil;
import scala.Tuple2;

import java.util.Date;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapRowTo;
import static com.datastax.spark.connector.japi.CassandraJavaUtil.mapToRow;

public class MetricAggregator {

    /**
     * Method to read RDD with records from cassandra database
     * @param context Job spark context
     * @return RDD with records information
     */
    public static JavaRDD<InputMetric> getMetrics(JavaSparkContext context)
    {
        return CassandraJavaUtil.javaFunctions(context)
                // Use keyspace "timeseries", table "raw_metrics" and map result to InputMetric class
                .cassandraTable("timeseries", "raw_metrics", mapRowTo(InputMetric.class))
                .select("id", "time", "value");
    }

    /**
     * Method to aggregate values per ID and time frame
     * @param rawMetricsRDD RDD with records information
     * @return RDD with aggregated metrics
     */
    public static JavaRDD<AggregatedMetric> aggregateMetrics(JavaRDD<InputMetric> rawMetricsRDD, String scale)
    {
        TimeUtil util = new TimeUtil(scale);
        return rawMetricsRDD
                // Round message time
                .map((metric) -> new InputMetric(metric.getId(), new Date(util.roundTime(metric.getTime().getTime())), metric.getValue()))
                // Map log info to key-value pairs, using (time, id), and (value, integer 1 (count)) as value
                .mapToPair((metric) -> new Tuple2<>(new Tuple2<>(metric.getId(), metric.getTime()), new Tuple2<>(metric.getValue(), 1)))
                // Reduce key-value pairs to sum all values per key
                .reduceByKey((x, y) -> new Tuple2<>(x._1 + y._1, x._2 + y._2))
                // Map key-value pairs to output class objects, averaging value with count
                .map((tuple) -> new AggregatedMetric(tuple._1._1, tuple._1._2, scale, tuple._2._1/tuple._2._2));
    }

    /**
     * Save metrics results back to cassandra database
     * @param context Job spark context
     * @param metricsRDD RDD with aggregated metrics
     */
    public static void saveMetrics(JavaSparkContext context, JavaRDD<AggregatedMetric> metricsRDD)
    {
        // Create a connector to cassandra database for drier program
        CassandraConnector connector = CassandraConnector.apply(context.getConf());

        // Create a table for output
        try (Session session = connector.openSession()) {
            session.execute("CREATE TABLE IF NOT EXISTS timeseries.aggregated_metrics (id int, time timestamp," +
                    "scale varchar, value int, PRIMARY KEY (id, time))");
        }

        // Map RDD to a table and write it to the DB
        CassandraJavaUtil.javaFunctions(metricsRDD)
                .writerBuilder("timeseries", "aggregated_metrics", mapToRow(AggregatedMetric.class))
                .saveToCassandra();

    }
}
