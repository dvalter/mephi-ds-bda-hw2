package org.bitbucket.dvalter.dsbda.hw2;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.bitbucket.dvalter.dsbda.hw2.dto.AggregatedMetric;
import org.bitbucket.dvalter.dsbda.hw2.dto.InputMetric;
import org.bitbucket.dvalter.dsbda.hw2.timeseries.MetricAggregator;

/**
 * Aggregates values for the id within a time frame
 * @author Dmitry Valter
 * @since 7/11/2020.
 */
@Slf4j
public class SparkApplication {

    /**
     * @param args - args[0]: time patters
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            throw new RuntimeException("Usage: spark-submit --class <...>.SparkApplication <FILE>.jar scale");
        }

        String scale = args[0];
        // Get spark master url from environment variables
        String sparkMasterUrl = System.getenv("SPARK_MASTER_URL");
        if(StringUtils.isBlank(sparkMasterUrl))
        {
            // If required environment variable is not set, throw exception ending the application
            throw new IllegalStateException("SPARK_MASTER_URL environment variable must be set.");
        }

        // Get cassandra url from environment variables
        String cassandraUrl = System.getenv("CASSANDRA_URL");
        if(StringUtils.isBlank(cassandraUrl))
        {
            // If required environment variable is not set, throw exception ending the application
            throw new IllegalStateException("CASSANDRA_URL environment variable must be set");
        }

        // Create spark configuration
        SparkConf sparkConf = new SparkConf()
                // Set application name
                .setAppName("timeseries")
                // Set spark master url
                .setMaster(sparkMasterUrl)
                // Set cassandra url to provided url
                .set("spark.cassandra.connection.host", cassandraUrl)
                // Set flag to INSERT to a single cassandra node
                .set("spark.cassandra.output.consistency.level", "LOCAL_ONE");

        // Create a spark context
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        // Extract RDD with input data from cassandra
        JavaRDD<InputMetric> input = MetricAggregator.getMetrics(sparkContext);
        // Transform data
        JavaRDD<AggregatedMetric> output =MetricAggregator.aggregateMetrics(input, scale);
        // Write output data to cassandra
        MetricAggregator.saveMetrics(sparkContext, output);
    }
}
