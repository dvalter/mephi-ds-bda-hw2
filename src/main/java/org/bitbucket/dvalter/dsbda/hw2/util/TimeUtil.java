package org.bitbucket.dvalter.dsbda.hw2.util;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

public class TimeUtil implements Serializable {
    private long scale;

    /**
     * Validate scale format
     *
     * @param scale Scale for aggregation
     */
    private static void validateScale(String scale) throws IllegalArgumentException {
        if (!scale.toLowerCase().matches("\\d+ms") &&
                !scale.toLowerCase().matches("\\d+s") &&
                !scale.toLowerCase().matches("\\d+m") &&
                !scale.toLowerCase().matches("\\d+h") &&
                !scale.toLowerCase().matches("\\d+d")) {
            throw new IllegalArgumentException("Illegal scale format '" + scale + "'");
        }
    }

    /**
     * Initializes parser with a scale
     *
     * @param stringScale string stale to parse and use
     */
    public TimeUtil(String stringScale) {
        validateScale(stringScale);
        if (stringScale.endsWith("ms")) {
            scale = Integer.parseInt(stringScale.replace("ms", ""));
        } else if (stringScale.endsWith("s")) {
            scale = TimeUnit.SECONDS.toMillis(Integer.parseInt(stringScale.replace("s", "")));
        } else if (stringScale.endsWith("m")) {
            scale = TimeUnit.MINUTES.toMillis(Integer.parseInt(stringScale.replace("m", "")));
        } else if (stringScale.endsWith("h")) {
            scale = TimeUnit.HOURS.toMillis(Integer.parseInt(stringScale.replace("h", "")));
        } else if (stringScale.endsWith("d")) {
            scale = TimeUnit.DAYS.toMillis(Integer.parseInt(stringScale.replace("d", "")));
        }
    }

    /**
     * Rounds timestamp to a given sca;e
     *
     * @param timestamp raw timestamp
     * @return rounded timestamp
     */
    public long roundTime(long timestamp) {
        // calculate reminder
        long mod = timestamp % scale;
        // result would be the reminder subtracted from the precise value
        long res = timestamp - mod;

        return res;
    }
}
