package org.bitbucket.dvalter.dsbda.hw2.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Raw value
 * @author Dmitry Valter
 * @since 7/11/2020.
 */
@Data
@AllArgsConstructor
public class InputMetric implements Serializable {

    // ID
    private Integer id;

    // timestamp
    private Date time;

    // value
    private Integer value;
}