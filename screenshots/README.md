Screenshots
===========

## Screenshot of successfully executed test

![Tests with maven](./SuccessfullyExecutedTests.png)

## Screenshot of successfully executed job

![Executed job](./SuccessfullyExecutedJob.png)

## Screenshot of results

![Results](./Results.png)
