#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'You should specify input file!'
    exit 1
fi


docker cp "$1" docker_clickhouse_1:/tmp
docker exec -it docker_clickhouse_1 bash -c "cat \"/tmp/$(basename "$1")\" | /usr/bin/clickhouse-client --query \"INSERT INTO logs.timeseries FORMAT CSV\""
