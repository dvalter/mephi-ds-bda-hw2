#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'You should specify input file!'
    exit 1
fi

year=$(date +%Y)
month=$(date +%m)

function genRow
{
    month=$(( 1 + RANDOM % 12 ))
    day=$(( 1 + RANDOM % 29 ))
    hour=$(( RANDOM % 24 ))
    minute=$(( RANDOM % 60 ))
    second=$(( RANDOM % 60 ))
    id=$(( RANDOM % 255 ))
    value="$(( RANDOM % 100 ))0"
    timestamp="$(date +%s -d "$year-$month-$day $hour:$minute:$second")000"
    echo $id, $timestamp, $value
}

for i in `seq $1`
do
    genRow
done
