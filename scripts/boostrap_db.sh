#!/bin/bash

# create clickhouse DB and table
docker exec -it docker_clickhouse_1 bash -c 'clickhouse-client --query "CREATE DATABASE IF NOT EXISTS logs"'
docker exec -it docker_clickhouse_1 bash -c "clickhouse-client --query 'CREATE TABLE IF NOT EXISTS logs.timeseries ( \`id\` UInt32, \`time\` UInt64, \`value\` Int32 ) ENGINE = MergeTree() ORDER BY (time, id) SETTINGS index_granularity = 8192'"

# create cassandra DB and table
docker exec -it docker_cassandra_1 bash -c "cqlsh localhost -e \"CREATE KEYSPACE IF NOT EXISTS timeseries WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 3};\""
docker exec -it docker_cassandra_1 bash -c "cqlsh localhost -e \"CREATE TABLE IF NOT EXISTS timeseries.raw_metrics (id int, time timestamp,  value int, PRIMARY KEY (id, time));\""

